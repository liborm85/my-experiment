<?php

var_dump(setlocale(LC_ALL, 0));
var_dump(mb_internal_encoding('UTF-8'));
//var_dump(setlocale(LC_ALL, 'cs_CZ.UTF-8'));
var_dump(setlocale(LC_ALL, 'C.UTF-8'));
var_dump(setlocale(LC_ALL, 0));

$original = 'ABCabcěščřžýáíéĚŠČŘŽÝÁÍÉ1234567890_X-X/X\\X,X.X/X;X[X]X`X-X=X';
$expected = 'abcabcescrzyaieescrzyaie1234567890_x-x-x-x-x-x-x-x-x-x-x-x-x';


exec('locale -a', $locs);
foreach ($locs as $loc) {
    var_dump($loc);
}

echo $original; echo "\n";

$text = $original;

// odstranění diakritiky
$text = iconv("utf-8", 'us-ascii//TRANSLIT', $text);

echo $text;
echo "\n";

// nahrazení pomlčkami znaků jiných než písmen nebo čísel
$text = preg_replace('~[^\\pL0-9_]+~u', '-', $text);

// odstranění pomlček na začátku a konci
$text = trim($text, '-');

// malá písmena
$text = strtolower($text);

echo $text;
echo "\n";
// pouze a-z, 0-9, - a _
$actual = preg_replace('~[^-a-z0-9_]+~', '', $text);

echo $actual;
echo "\n";
echo $expected;

echo "\n";
echo "\n";

if ($actual <> $expected) {
    throw new \Exception('FAIL');
}

